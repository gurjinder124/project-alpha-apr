from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class TaskCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "due_date",
        "is_completed",
    )
